//
//  ViewController.swift
//  stopWatch Project
//
//  Created by Click Labs on 1/15/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var timer = NSTimer()
    var mSecondCounter = 00 // variable to display mni seconds
    var secCounter = 00 // variable to display seconds
    var minCounter = 00 // variable to displa minutes
    var isPlayClicked = false // variavle used to check the play button
    var splitCounter = 1 //to handel the lap labels (3 lap labels used)
    
    @IBOutlet weak var watchLabel: UILabel!
    
    @IBAction func playAction(sender: AnyObject) {
        
        if isPlayClicked == false {
            
            timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: "watchAction", userInfo: nil, repeats: true)
            isPlayClicked = true
        }
    }

    @IBAction func pauseAction(sender: AnyObject) {
        
        timer.invalidate()
        isPlayClicked = false
    }
    
    @IBAction func cancelAction(sender: AnyObject) {
        
        timer.invalidate()
        mSecondCounter = 00
        secCounter = 00
        minCounter = 00
        watchLabel.text = "00:00:00"
        isPlayClicked = false
        splitLabelOne.hidden = true
        splitLabelTwo.hidden = true
        splitLAbelThree.hidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func watchAction() { // This function is called after ever 0.1 seconds by the timer
    
        mSecondCounter++
        
        if mSecondCounter % 10 == 0 {
            
            secCounter++
            mSecondCounter = 00
        }
        
        if secCounter % 60 == 0 && secCounter != 0 {
            
            minCounter++
            secCounter = 00
        }
        
        watchLabel.text = String(minCounter) + ":" + String(secCounter) + ":" + String(mSecondCounter)
    }

    // Split time labels
    @IBOutlet weak var splitLabelOne: UILabel!
    @IBOutlet weak var splitLabelTwo: UILabel!
    @IBOutlet weak var splitLAbelThree: UILabel!
    
    //Action od Record Button
    @IBAction func splitAction(sender: AnyObject) {
        
        // Condition to display split labels based on value of splitCounter
        
        if splitCounter == 1 {
        
            splitLabelOne.text = "Lap 1: " + String(minCounter) + ":" + String(secCounter) + ":" + String(mSecondCounter)
            splitLabelOne.hidden = false
            splitLabelTwo.hidden = true
            splitLAbelThree.hidden = true
        }
        
        else if splitCounter == 2{
            
            splitLabelTwo.text = "Lap 2: " + String(minCounter) + ":" + String(secCounter) + ":" + String(mSecondCounter)
            splitLabelTwo.hidden = false
        }
        
        else if splitCounter == 3{
            
            splitLAbelThree.text = "Lap 3: " + String(minCounter) + ":" + String(secCounter) + ":" + String(mSecondCounter)
            splitLAbelThree.hidden = false
        }
        
        splitCounter++
    
        if splitCounter == 4 { // condition used to set the lapCounter value to 1
            
            splitCounter = 1
        }
    }
}

