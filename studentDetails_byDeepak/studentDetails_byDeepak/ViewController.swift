//
//  ViewController.swift
//  studentDetails_byDeepak
//
//  Created by Click Labs on 1/13/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // global Variables
    var isDisplayVisible = false
    
    // Input text fields
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var ageField: UITextField!
    @IBOutlet weak var classField: UITextField!
    @IBOutlet weak var mathsField: UITextField!
    @IBOutlet weak var englishField: UITextField!
    @IBOutlet weak var scienceField: UITextField!
    @IBOutlet weak var historyfield: UITextField!
    @IBOutlet weak var musicField: UITextField!
    
    var fieldArr = ["nameField", "ageField", "classField"]
    
    // LABELS TO DISPLAY
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var classLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var mathsLabel: UILabel!
    @IBOutlet weak var englishLabel: UILabel!
    @IBOutlet weak var scienceLabel: UILabel!
    @IBOutlet weak var historyLabel: UILabel!
    @IBOutlet weak var musicLabel: UILabel!
    @IBOutlet weak var totakMarksLabel: UILabel!
    @IBOutlet weak var avgMarksLabel: UILabel!
    
    @IBOutlet weak var imageDisplay: UIImageView!
    
    @IBOutlet weak var resultDisplayView: UIView!
   
    @IBAction func generateResultAction(sender: AnyObject) {
        
        self.view.endEditing(true);
        
        // checking if aff fields are filled
        if nameField.text == "" || ageField.text == "" || classField.text == "" || mathsField.text == "" || englishField.text == "" || scienceField.text == "" || historyfield.text == "" && musicField.text == "" {
            
            println("invalid input \nPlease enter all fields")
            
            if nameField.text == "" {
                
                nameField .becomeFirstResponder()
                println("name Fiels empty")
            }
            
            else if classField.text == "" {
                
                classField .becomeFirstResponder()
                println("Class Fiels empty")
            }
            
            else if ageField.text == "" {
                
                ageField .becomeFirstResponder()
                println("Age Fiels empty")
            }
    
            else if mathsField.text == "" {
                
                mathsField .becomeFirstResponder()
                println("Maths Fiels empty")
            }
                
            else if englishField.text == "" {
                
                englishField .becomeFirstResponder()
                println("English Fiels empty")
            }
                
            else if scienceField.text == "" {
                
                scienceField .becomeFirstResponder()
                println("Science Fiels empty")
            }
                
            else if historyfield.text == "" {
                
                historyfield .becomeFirstResponder()
                println("History Fiels empty")
            }
                
            else {
                
                musicField .becomeFirstResponder()
                println("Music Fiels empty")
            }
            
        } else if nameField.text != "" && ageField.text != "" && classField.text != "" && mathsField.text != "" && englishField.text != "" && scienceField.text != "" && historyfield.text != "" && musicField.text != ""{
            
            if (isDisplayVisible == false) {
                
                resultDisplayView.hidden = false
                isDisplayVisible = true
                println("displaying view")
            }
            
        //displaying the content of text fields in labels
        nameLabel.text = "Name :" + " " + nameField.text
        ageLabel.text = "Age :" + " " + ageField.text
        classLabel.text = "Class :" + " " + classField.text
        mathsLabel.text = "Maths :" + " " + mathsField.text
        englishLabel.text = "Engligh :" + " " + englishField.text
        scienceLabel.text = "Science :" + " " + scienceField.text
        historyLabel.text = "History :" + " " + historyfield.text
        musicLabel.text = "Music :" + " " + musicField.text
        
        //Put the value of marks fields in variables
        var marksInMaths:Int = mathsField.text.toInt()!
        var marksInEnglish:Int = englishField.text.toInt()!
        var marksInScience:Int = scienceField.text.toInt()!
        var marksInHistory:Int = historyfield.text.toInt()!
        var marksInMusic:Int = musicField.text.toInt()!
        
        var totalMarks:Int = marksInMaths + marksInEnglish + marksInScience + marksInHistory + marksInMusic
        var avgmarks:Double = Double(totalMarks) / 5
        
        totakMarksLabel.text = "Total: \(totalMarks)"
        avgMarksLabel.text = "Average: \(avgmarks)"

         if avgmarks >= 80.0 { //condition to display pic according to average
            
            var imageGold : UIImage = UIImage(named:"gold.jpeg")!
            imageDisplay.image = imageGold
                        println("Gold Star")
        }
        
       else if avgmarks >= 60.0 && avgmarks < 80 {
            
            var imageSilver : UIImage = UIImage(named:"silver.jpeg")!
            imageDisplay.image = imageSilver
            println("Silover Star")
        }
        
        else if avgmarks < 60 {
            
            var imageBronze : UIImage = UIImage(named:"bronze.jpg")!
            imageDisplay.image = imageBronze
            println("Bronze Star")
          }
       }
    }
    
    @IBAction func resetAction(sender: AnyObject) {
        
        self.view.endEditing(true);
        
        nameField.text = nil
        classField.text = nil
        ageField.text = nil
        mathsField.text = nil
        englishField.text = nil
        scienceField.text = nil
        historyfield.text = nil
        musicField.text = nil
        
        if resultDisplayView.hidden == false { // hide-unHide the displayView
            
            resultDisplayView.hidden = true
            isDisplayVisible = false
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
    }
}

// Called when 'return' key pressed. return NO to ignore.
func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
}

