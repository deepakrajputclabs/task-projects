//
//  ViewController.swift
//  calculator_byDeepak
//
//  Created by Click Labs on 1/12/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    // Global Variables initialised
    var firstNumber = 0.0
    var secondNumber = 0.0
    var result = 0.0
    var operation = ""
    
    //Boolen Variables
    var isTypingNumber = false //to handel addition of strings
    var isDotPressed = false //to handel the .
    var operationContinued = false // to handle continuous operations
    var isOperationClicked = false // to handle when operation clicked simultaneously
    
    @IBOutlet weak var calculatorDisplay: UITextField!

    // Action of operation buttons
    @IBAction func plusButton(sender: AnyObject) {
     
        
        if isOperationClicked == false {
        
        isDotPressed = false
        
        
        if operationContinued == false { // Check if operation is clicked 1st time
            
        isTypingNumber = false
        firstNumber = (calculatorDisplay.text as NSString).doubleValue
        operation = "+"
        calculatorDisplay.text = ""
        operationContinued = true
            
        }
            
        else if operationContinued == true {
            
            equalButton(isTypingNumber) // If operation is contined without pressing "=" then action of "=" button is called
            isTypingNumber = false
            firstNumber = result // If operations are pressed continuosly without pressing "=" then first result of previous transaction is assigned to "firstNumber"
            operation = "+"
            operationContinued = true
            }
           isOperationClicked = true
        }
        operation = "+" //change made
    }
    
    // The action of the Minus, Multiply, divide buttons are similar
    
    @IBAction func minusButton(sender: AnyObject) {
        
        if isOperationClicked == false {
        
        isDotPressed = false

         if operationContinued == false {
            
            isTypingNumber = false
            firstNumber = (calculatorDisplay.text as NSString).doubleValue
            operation = "-"
            calculatorDisplay.text = ""
            operationContinued = true
        }
        else if operationContinued == true{
            
            equalButton(isTypingNumber)
            isTypingNumber = false
            firstNumber = result
            operation = "-"
            operationContinued = true
        }
    }
        isOperationClicked = true
        operation = "-" //change made
    }
    
    @IBAction func multiplyButton(sender: AnyObject) {
        
        if isOperationClicked == false {
        
        isDotPressed = false
        
        if operationContinued == false {
            
            isTypingNumber = false
            firstNumber = (calculatorDisplay.text as NSString).doubleValue
            operation = "*"
            calculatorDisplay.text = ""
            operationContinued = true
        }
        else if operationContinued == true{
            
            equalButton(isTypingNumber)
            isTypingNumber = false
            firstNumber = result
            operation = "*"
            operationContinued = true
        }
    }
        isOperationClicked = true
        operation = "*" //change made
    }
    
    @IBAction func divideButton(sender: AnyObject) {
    
        if isOperationClicked == false {
            
            isDotPressed = false
            
            if operationContinued == false { // Check if operation is clicked 1st time
                
                isTypingNumber = false
                firstNumber = (calculatorDisplay.text as NSString).doubleValue
                operation = "/"
                calculatorDisplay.text = ""
                operationContinued = true
            }
            else if operationContinued == true {
                
                equalButton(isTypingNumber) // If operation is contined without pressing "=" then action of "=" button is called
                isTypingNumber = false
                firstNumber = result // If operation is contined without pressing "=" then first result of previous transaction is assigned to "firstNumber"
                operation = "/"
                operationContinued = true
            }
            isOperationClicked = true
        }
        operation = "/" //change made
    }
    
    @IBAction func dodButton(sender: AnyObject) {
        
        if isDotPressed == false{
        var number = "."
        
        if isTypingNumber {
            calculatorDisplay.text = calculatorDisplay.text + number
        }
        else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }
        }
        isDotPressed = true
    }
    
    @IBAction func dig0(sender: AnyObject) {
        
        var number = "0"

        if isTypingNumber { // to display text in field (aditon of strings)
            calculatorDisplay.text = calculatorDisplay.text + number
        } else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }
        isOperationClicked = false
    }
    
    // Action of all the buttons 1 to 9 are similar to previous one
    @IBAction func dig1(sender: AnyObject) {
        
        var number = "1"
        
        if isTypingNumber {
            calculatorDisplay.text = calculatorDisplay.text + number
        } else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }
        isOperationClicked = false
    }
    
    @IBAction func dig2(sender: AnyObject) {
        
        var number = "2"
        
        if isTypingNumber {
            calculatorDisplay.text = calculatorDisplay.text + number
        } else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }
        isOperationClicked = false
    }
    
    @IBAction func dig3(sender: AnyObject) {
        
        var number = "3"
        
        if isTypingNumber {
            calculatorDisplay.text = calculatorDisplay.text + number
        } else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }
        
        isOperationClicked = false

    }
    
    @IBAction func dig4(sender: AnyObject) {
        
        var number = "4"
        
        if isTypingNumber {
            calculatorDisplay.text = calculatorDisplay.text + number
        } else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }
        isOperationClicked = false
    }
    
    @IBAction func dig5(sender: AnyObject) {
        
        var number = "5"
        
        if isTypingNumber {
            calculatorDisplay.text = calculatorDisplay.text + number
        } else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }
        isOperationClicked = false
    }
    
    @IBAction func dig6(sender: AnyObject) {
        
        var number = "6"
        
        if isTypingNumber {
            calculatorDisplay.text = calculatorDisplay.text + number
        } else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }
        isOperationClicked = false
    }
    
    @IBAction func dig7(sender: AnyObject) {
        
        var number = "7"
        
        if isTypingNumber {
            calculatorDisplay.text = calculatorDisplay.text + number
        } else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }
        isOperationClicked = false
    }
    
    @IBAction func dig8(sender: AnyObject) {
        
        var number = "8"
        
        if isTypingNumber {
            calculatorDisplay.text = calculatorDisplay.text + number
        } else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }
        isOperationClicked = false
    }
    
    @IBAction func dig9(sender: AnyObject) {
        
        var number = "9"
        
        if isTypingNumber {
            calculatorDisplay.text = calculatorDisplay.text + number
        } else {
            calculatorDisplay.text = number
            isTypingNumber = true
        }
        isOperationClicked = false
    }
    
    // Action of equal to button
    @IBAction func equalButton(sender: AnyObject) {
        
        isTypingNumber = false
        secondNumber = (calculatorDisplay.text as NSString).doubleValue
        
        if operation == "+" {
            
            result = firstNumber + secondNumber
            
        } else if operation == "-" {
            
            result = firstNumber - secondNumber
        
        } else if operation == "*" {
            
            result = firstNumber * secondNumber
            
        } else if operation == "/" {
            
            if secondNumber != 0 {
                
            result = firstNumber / secondNumber
                
            } else if secondNumber == 0 {
                
                println("invalid input")

            }
        }

        //firstNumber = result
        calculatorDisplay.text = "\(result)"
        isOperationClicked = false
        operationContinued = false
        isDotPressed = false
    }
    
    @IBAction func clearButton(sender: AnyObject) {
        
        calculatorDisplay.text = ""
        operation = ""
        isTypingNumber = false
        firstNumber = 0.0
        secondNumber = 0.0
        isDotPressed = false
        operationContinued = false
        result = 0.0
        isOperationClicked = false
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
}

