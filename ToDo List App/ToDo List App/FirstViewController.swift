//
//  FirstViewController.swift
//  ToDo List App
//
//  Created by Click Labs on 1/20/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

var toDoArray: [String] = [] // This is the array whose content is displayed in the table. Declared global so for sigue purpose

class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var myTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int { // This function returns the number of rows in the table
        
        return toDoArray.count // Number of rows is equal to the count of toDoArray
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell { // This function is for the content of the individual cells of the table
        
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell") // Declaring the cell
        cell.textLabel?.text = toDoArray[indexPath.row] // Adding the content of the toDoarray to cells
        cell.backgroundColor = UIColor.darkGrayColor() //Set the bg color of the cells
        cell.textLabel?.textColor = UIColor.whiteColor() // Set the text color
        
        return cell
    }
    
    override func viewWillAppear(animated: Bool) { // This function executes before the view loads
        
        if var toDoList: AnyObject = NSUserDefaults.standardUserDefaults().valueForKey("tab") { // This condition is used to update the content of table. The value of user defaults is put in anothe array toDoList
            
            toDoArray = [] // Emptying the toDoArray
            
            for var i = 0; i < toDoList.count; ++i { // This loop is used to put the content on toDoList in toDoArray
                
                toDoArray.append(toDoList[i] as NSString)
            }
        }
    }

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) { // This function is used to delete the rows of the table
        
        if editingStyle == UITableViewCellEditingStyle.Delete {
            
            toDoArray.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            
            // Updating the value of user defaults
            let fixedToDoArray = toDoArray
            NSUserDefaults.standardUserDefaults().setObject(fixedToDoArray, forKey: "tab")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
}

