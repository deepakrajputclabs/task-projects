//
//  SecondViewController.swift
//  ToDo List App
//
//  Created by Click Labs on 1/20/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITextFieldDelegate {
  
    @IBOutlet weak var toDoInput: UITextField!
    
    @IBAction func addButtonAction(sender: AnyObject) { //This function is invoked when Add button is clicked
        
        toDoArray.append(toDoInput.text) // Text of the input text field is added to toDoArray
        
        // The followin lines saves the array in user defaults
        let fixedToDoArray = toDoArray // content of todoArray is put in an immutable array so that it can be saved in user default
        NSUserDefaults.standardUserDefaults().setObject(fixedToDoArray, forKey: "tab")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        toDoInput.text = "" // Clearing the input field
        self.view.endEditing(true) // Hide the key board
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    func textFieldShouldClear(textField: UITextField) -> Bool { // This function is invoked when clear button on keyboard is pressed
        
        toDoInput.resignFirstResponder() // Hide the key board
        return true
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) { // This function is invoked when user touches anywhere on screen
       
        self.view.endEditing(true) // Hide the key board
    }
}

