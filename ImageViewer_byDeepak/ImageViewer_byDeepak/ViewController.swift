//
//  ViewController.swift
//  ImageViewer_byDeepak
//
//  Created by Click Labs on 1/13/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var inputNumber: UITextField!
    
    @IBOutlet weak var imagePlane: UIImageView!
  
    @IBAction func submitAction(sender: AnyObject) {
        
        self.view.endEditing(true)
        
        var number = inputNumber.text
        
        switch number {
            
        case "1":
            
            var image = UIImage(named:"img1.png")
            imagePlane.image = image
            
        case "2":
    
            var image = UIImage(named:"img2.jpeg")
            imagePlane.image = image
            
        case "3":
        
            var image = UIImage(named:"gold.jpeg")
            imagePlane.image = image
            
        case "4":
            
            var image = UIImage(named:"img3.jpeg")
            imagePlane.image = image
            
        case "5":
            
            var image = UIImage(named:"img4.jpg")
            imagePlane.image = image
            
        case "6":
            
            var image = UIImage(named:"img5.jpeg")
            imagePlane.image = image
            
        case "7":

            var image = UIImage(named:"silver.jpeg")
            imagePlane.image = image
            
        case "8":
            
            var image = UIImage(named:"img7.jpeg")
            imagePlane.image = image
            
        case "9":
            
            var image = UIImage(named:"bronze.jpg")
            imagePlane.image = image
            
        case "10":
            
            var image = UIImage(named:"img3.jpeg")
            imagePlane.image = image
            
        default:
            
            var image = UIImage(named:"img1.png")
            imagePlane.image = image
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

