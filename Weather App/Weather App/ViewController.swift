//
//  ViewController.swift
//  Weather App
//
//  Created by Click Labs on 1/21/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit
import Foundation


class ViewController: UIViewController {
    
    var initialWeatherMessage = "Sunshine is delicious, rain is refreshing, wind braces us up, snow is exhilarating; there is really no such thing as bad weather, only different kinds of good weather"
    var weatherUpdateString = "default" // This string gets the value of the updated weather of the city or the error message
    
    @IBOutlet weak var cityInputField: UITextField!
    @IBOutlet weak var weatherDisplayMessage: UILabel!
    @IBOutlet weak var minTempDisplay: UILabel!
    @IBOutlet weak var maxTempDisplay: UILabel!
    
    @IBAction func findWeatherPressed(sender: AnyObject) {
        
        self.view.endEditing(true) // Hides the key board when button is pressed
        
        var urlString = "http://www.weather-forecast.com/locations/" + cityInputField.text.stringByReplacingOccurrencesOfString(" ", withString: "") + "/forecasts/latest" // url in the form <url> <city name> <url>
        var url = NSURL (string: urlString) // typecast url in form NSURL
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) { (data, response, erroer) in // Create a URL Session
            
            var urlContent = NSString(data: data, encoding: NSUTF8StringEncoding) // Encode UTF8 format
            var contentArray = urlContent!.componentsSeparatedByString("<span class=\"phrase\">") // breakes string in two parts. One before and other after the text in between("")
            
            if NSString(string: urlContent!).containsString("<span class=\"phrase\">") { //condition so that the app doen not crash in case of an invalid input.
                
                var newContentArray = contentArray[1].componentsSeparatedByString("</span") //breakes string in two parts. One before and other after the text in between("")
                
                // Using the above lojic max and min temperature is obtained in form of NSString
                var maxTemp = newContentArray[0].componentsSeparatedByString("max")
                var newMaxTemp = maxTemp[1].componentsSeparatedByString("&deg;")
                var minTemp = newContentArray[0].componentsSeparatedByString(", min")
                var newMinTemp = minTemp[1].componentsSeparatedByString("&deg;")
                
                self.weatherUpdateString = newContentArray[0].stringByReplacingOccurrencesOfString("&deg;", withString: "º") as NSString // Replace &deg; by º
                
                dispatch_async(dispatch_get_main_queue()){
                    
                    // Printing the labels
                    self.weatherDisplayMessage.text = self.weatherUpdateString
                    self.minTempDisplay.text = "\(newMinTemp[0]) ºC"
                    self.maxTempDisplay.text = "\(newMaxTemp[0]) ºC"
                }
                
            } else {
            
                self.weatherUpdateString = "City not found ! \nEnter the Text Field Again"
                dispatch_async(dispatch_get_main_queue()){
                    
                    // Printing the labels
                    self.weatherDisplayMessage.text = self.weatherUpdateString
                    self.minTempDisplay.text = "00"
                    self.maxTempDisplay.text = "00"
                }
            }
        }
            task.resume()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.weatherDisplayMessage.text = initialWeatherMessage // This is the initial message which is displayed when view load
    }
}

